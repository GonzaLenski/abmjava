
public class Cliente {
  
	private String nombre;
	private String apellido;
	private int edad;
	private String direccion;
	private int telefono;
	private String mail;
	private int id;
	
	
	
	public Cliente(String nombre, String apellido, String direccion, int telefono, String mail, int edad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.telefono = telefono;
		this.mail = mail;
		this.edad = edad;
	}


	public String getNombre() {
		return nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public String getDireccion() {
		return direccion;
	}


	public int getTelefono() {
		return telefono;
	}


	public String getMail() {
		return mail;
	}


	public int getEdad() {
		return edad;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
	
}
