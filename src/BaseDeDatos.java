
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BaseDeDatos {

	public static void agregarClienteALaTabla(String nombre, String apellido, int edad, String direccion, int telefono,
			String mail) throws SQLException, ClassNotFoundException {
		
		try {
			
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abmjava";
			Class.forName(myDriver);
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "insert into cliente (nombre, apellido, edad, direccion, telefono, mail)"
					+ " values (?, ?, ?, ?, ?, ?)";
		
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, nombre);
			preparedStmt.setString(2, apellido);
			preparedStmt.setInt(3, edad);
			preparedStmt.setString(4, direccion);
			preparedStmt.setInt(5, telefono);
			preparedStmt.setString(6, mail);

			preparedStmt.executeUpdate();
			conn.close();
			
		} catch (SQLException e) {
			System.out.println("Se ha generado la siguiente excepci�n:");
			System.out.println(e.getMessage());
		}
	}
       
	public static void updateClientePorID(int id, String nombre, String apellido, int edad, String direccion,
			int telefono, String mail) throws SQLException, ClassNotFoundException {
		try {
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abmjava";
			Class.forName(myDriver);
		
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			
			String query = "UPDATE cliente SET nombre = ?, apellido = ?, telefono=? , mail = ?, direccion = ?, edad = ?  WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, nombre);
			ps.setString(2, apellido);
			ps.setInt(3, telefono);
			ps.setString(4, mail);
			ps.setString(5, direccion);
			ps.setInt(6, edad);
			ps.setInt(7, id);

			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Se ha generado la siguiente excepci�n:");
			System.out.println(e.getMessage());

		}
	}

	public static void borrarTodosLosRegistrosDeLaTablaClientes() throws SQLException, ClassNotFoundException {
		try {

			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abmjava";
			Class.forName(myDriver);

			Connection conn = DriverManager.getConnection(myUrl, "root", "");

			String sql = "DELETE FROM cliente";
			PreparedStatement prest = conn.prepareStatement(sql);
			prest.executeUpdate();
			conn.close();

		} catch (SQLException s) {
			System.out.println("No fue posible ejecutar el SQL");
		}
	
}}
